<?php
// $Id: page.tpl.php,v 1.15 2010/11/20 04:03:51 webchick Exp $
?>
        <div class="mainView">
            <div id="contentPane">
                <div id="section-0" style="top: 0pt;" class="shrinkWrap">
                    <div class="sectionHeader">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="nytlogo">
                                    </td>
                                    <td class="line">
                                        <div class="verticalLine">
                                        </div>
                                    </td>
                                    <td class="sectionName">
                                        Top News
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="rightHeader">
                            <div class="loginRequest">
                            </div>
                            <div class="breakingNews">
                            </div>
                        </div>
                    </div>
                    <div class="pageScrollContainer">
                        <ul class="blocks grid">
                            <li class="story">
                                <div class="container fill">
                                    <div class="content">
                                        <div class="headline">
                                            <a href="#">Madonnaβ€™s Charity Fails in Bid to Finance School</a>
                                        </div>
                                        <div class="byline">
                                            By ADAM NAGOURNEY
                                        </div>
                                        <div class="thumbnail">
                                            <img width="60" height="60" src="images/post-thumb.jpg">
                                        </div>
                                        <div class="summary">
                                            <p>
                                                Plans to build the $15 million school have been abandoned amid criticism of what auditors called $3.8 million in outlandish expenditures.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="story">
                                <div class="container fill">
                                    <div class="content">
                                        <div class="headline">
                                            <a href="#">To End Inquiry, Suffolk Leader Exits Race</a>
                                        </div>
                                        <div class="byline">
                                            By DAVID M. HALBFINGER
                                        </div>
                                        <div class="thumbnail">
                                            <img width="60" height="60" src="images/post-thumb.jpg">
                                        </div>
                                        <div class="summary">
                                            <p>
                                                Steve Levy, the Suffolk County executive who had switched parties to seek the Republican nomination for governor, made the deal to end a criminal investigation into his political fund-raising.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="pageContent pages">
                            <ul id="page-0-0" class="blocks grid page first" style="left: 0px;">
                                <li class="story first">
                                    <div class="container fill">
                                        <div class="content">
                                            <div class="headline">
                                                <a href="#">Japan Quietly Evacuating a Wider Radius From Reactors</a>
                                            </div>
                                            <div class="byline">
                                                By DAVID JOLLY, HIROKO TABUCHI and KEITH BRADSHER
                                            </div>
                                            <div class="thumbnail">
                                                <img width="60" height="60" src="images/post-thumb.jpg">
                                            </div>
                                            <div class="summary">
                                                <p>
                                                    Officials said they would assist people who want to leave the area from 12 to 19 miles outside the plant, in a sign that they hold little hope of controlling it.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php print render($page['content']); ?>
                            </ul>
                            
                            
                        </div>
                    </div>
                    <div class="sectionFooter">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <div class="pageIndicators">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td class="left">
                                                            <div class="pageLeftIndicator" style="opacity: 0.3;">
                                                            </div>
                                                        </td>
                                                        <td class="center">
                                                            <div class="pageCirclesContainer">
                                                                ●○○○
                                                            </div>
                                                        </td>
                                                        <td class="right">
                                                            <div class="pageRightIndicator" style="opacity: 1;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		<div id="sidebar" class="">
			<dl class="sectionFeeds">
				<dt class="content" id="SectionsHeader"><div class="refresh">↺</div><div class="label">Sections</div></dt><dd class="content" id="SectionsContent" style="display: block; height: 426px;"><ol><li ordinal="0" class="selected"><span class="feedStatus">●</span><span class="label">Top News</span></li><li ordinal="1"><span class="feedStatus">●</span><span class="label">Opinion</span></li><li ordinal="2"><span class="feedStatus">●</span><span class="label">World</span></li><li ordinal="3"><span class="feedStatus">●</span><span class="label">U.S.</span></li><li ordinal="4"><span class="feedStatus">●</span><span class="label">Politics</span></li><li ordinal="5"><span class="feedStatus">●</span><span class="label">N.Y. / Region</span></li><li ordinal="6"><span class="feedStatus">●</span><span class="label">Business</span></li><li ordinal="7"><span class="feedStatus">●</span><span class="label">Technology</span></li><li ordinal="8"><span class="feedStatus">●</span><span class="label">Sports</span></li><li ordinal="9"><span class="feedStatus">●</span><span class="label">Science</span></li><li ordinal="10"><span class="feedStatus">●</span><span class="label">Health</span></li><li ordinal="11"><span class="feedStatus">●</span><span class="label" >Arts</span></li><li ordinal="12"><span class="feedStatus">●</span><span class="label">Book Review</span></li><li ordinal="13"><span class="feedStatus">●</span><span class="label">Fashion &amp; Style</span></li><li ordinal="14"><span class="feedStatus">●</span><span class="label">Weddings</span></li><li ordinal="15"><span class="feedStatus">●</span><span class="label">Dining &amp; Wine</span></li><li ordinal="16"><span class="feedStatus">●</span><span class="label">Sunday Magazine</span></li><li ordinal="17"><span class="feedStatus">●</span><span class="label">Week In Review</span></li><li ordinal="18"><span class="feedStatus">●</span><span class="label">Travel</span></li><li ordinal="19"><span class="feedStatus">●</span><span class="label">Obituaries</span></li><li ordinal="20"><span class="feedStatus">●</span><span class="label">Real Estate</span></li><li ordinal="21"><span class="feedStatus">●</span><span class="label">Most E-mailed</span></li></ol></dd>
			</dl>
			<dl class="helpers">
				<dt class="account">My Account</dt><dt class="customize">Customize</dt><dt class="shortcuts">Shortcuts</dt>
			</dl>
		</div>
        </div>
